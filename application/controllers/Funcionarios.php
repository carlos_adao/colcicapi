<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/libraries/REST_Controller.php';


class Funcionarios extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('funcionarios_model');
    }

    public function index_get()
    {
        $funcionarios = $this->funcionarios_model->get();

        if (!is_null($funcionarios)) {
           $this->response(array('Funcionario' => $funcionarios), 200);
        } else {
            $this->response(array('error' => 'Nao existe funcionarios na base de dados'), 404);
        }
    }

    public function find_get($id)
    {
        if (!$id) {
            $this->response(null, 400);
        }
        $funcionarios = $this->funcionarios_model->get($id);

        if (!is_null($funcionarios)) {
            $this->response(array('Funcionario' => $funcionarios), 200);
        } else {
            $this->response(array('error' => 'funcionario não encontrado'), 404);
        }
    }

    public function index_post()
    {
        if (!$this->post('funcionario')) {
            $this->response(null, 400);
        }

        $id = $this->funcionarios_model->save($this->post('funcionario'));

        if (!is_null($id)) {
            $this->response(array('Funcionario' => $id), 200);
        } else {
            $this->response(array('Erro ao salvar funcionario!!!'), 400);
        }
    }

    public function index_put()
    {
        if (!$this->put('funcionario')) {
            $this->response(null, 400);
        }

        $update = $this->funcionarios_model->update($this->put('funcionario'));

        if (!is_null($update)) {
            $this->response(array('Funcionario' => 'Dados Funcionario atualizado!'), 200);
        } else {
            $this->response(array('error', 'Não foi possivel atualizar os dados ...'), 400);
        }
    }

    public function index_delete($id)
    {
        if (!$id) {
            $this->response(null, 400);
        }

        $delete = $this->funcionarios_model->delete($id);

        if (!is_null($delete)) {
            $this->response(array('Funcionario' => 'Funcionario excluido'), 200);
        } else {
            $this->response(array('error', 'Não foi possivel excluir professor'), 400);
        }
    }
}
