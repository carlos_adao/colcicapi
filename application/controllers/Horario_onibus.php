<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/libraries/REST_Controller.php';


class Horario_onibus extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('horario_onibus_model');
    }

    public function index_get()
    {
        $horario = $this->horario_onibus_model->get();

        if (!is_null($horario)) {
            $this->response(array('Horario_onibus' => $horario), 200);
        } else {
            $this->response(array('error' => 'No hay horarios en la base de datos...'), 404);
        }
    }

    public function find_get($id)
    {
        if (!$id) {
            $this->response(null, 400);
        }
        $h = $this->hario_onibus_modelo->get($id);

        if (!is_null($h)) {
            $this->response(array('Horario_onibus' => $h), 200);
        } else {
            $this->response(array('error' => 'Horario no encontrada...'), 404);
        }
    }

    public function index_post()
    {
        if (!$this->post('h')) {
            $this->response(null, 400);
        }

        $id = $this->codhorario_onibus_model->save($this->post('h'));

        if (!is_null($id)) {
            $this->response(array('Horario_onibus' => $id), 200);
        } else {
            $this->response(array('error', 'Não encontrado no servidor...'), 400);
        }
    }

    public function index_put()
    {
        if (!$this->put('h')) {
            $this->response(null, 400);
        }

        $update = $this->codhorario_onibus_model->update($this->put('h'));

        if (!is_null($update)) {
            $this->response(array('Horario_onibus' => 'Horario stualizado actualizada!'), 200);
        } else {
            $this->response(array('error', 'Não encontrado no servidor...'), 400);
        }
    }

    public function index_delete($id)
    {
        if (!$id) {
            $this->response(null, 400);
        }

        $delete = $this->codhorario_onibus_model->delete($id);

        if (!is_null($delete)) {
            $this->response(array('Horario_onibus' => 'Horario eliminado!'), 200);
        } else {
            $this->response(array('error', 'Não encontrado no servidor...'), 400);
        }
    }
}
