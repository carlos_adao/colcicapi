<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/libraries/REST_Controller.php';


class Ementa extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('ementa_model');
    }

    public function index_get()
    {
        $ementas = $this->ementa_model->get();

        if (!is_null($ementas)) {
            $this->response(array('Ementae' => $ementas), 200);
        } else {
            $this->response(array('error' => 'Nao existe ementa_disciplina na base de dados'), 404);
        }
    }

    public function find_get($id)
    {
        if (!$id) {
            $this->response(null, 400);
        }
        $ementas = $this->ementa_model->get($id);

        if (!is_null($ementas)) {
            $this->response(array('Ementa' => $ementas), 200);
        } else {
            $this->response(array('error' => 'ementas não encontrado'), 404);
        }
    }

    public function index_post()
    {
        if (!$this->post('ementa_disciplina')) {
            $this->response(null, 400);
        }

        $id = $this->ementa_model->save($this->post('ementa_disciplina'));

        if (!is_null($id)) {
            $this->response(array('Ementa' => $id), 200);
        } else {
            $this->response(array('Erro ao salvar ementa_disciplina!!!'), 400);
        }
    }

    public function index_put()
    {
        if (!$this->put('ementa_disciplina')) {
            $this->response(null, 400);
        }

        $update = $this->ementa_model->update($this->put('ementa_disciplina'));

        if (!is_null($update)) {
            $this->response(array('Ementa' => 'Dados ementa_disciplina atualizado!'), 200);
        } else {
            $this->response(array('error', 'Não foi possivel atualizar os dados ...'), 400);
        }
    }

    public function index_delete($id)
    {
        if (!$id) {
            $this->response(null, 400);
        }

        $delete = $this->ementa_model->delete($id);

        if (!is_null($delete)) {
            $this->response(array('Ementa' => 'Ementa excluido'), 200);
        } else {
            $this->response(array('error', 'Não foi possivel excluir ementa_disciplina'), 400);
        }
    }
}
