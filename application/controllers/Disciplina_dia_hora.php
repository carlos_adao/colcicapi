<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/libraries/REST_Controller.php';


class Disciplina_dia_hora extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('disciplina_dia_hora_model');
    }

    public function index_get()
    {
        $disciplina_dia_hora = $this->disciplina_dia_hora_model->get();

        if (!is_null($disciplina_dia_hora)) {
            $this->response(array('Disciplina_dia_hora' => $disciplina_dia_hora), 200);
        } else {
            $this->response(array('error' => 'Nao existe disciplina_dia_hora na base de dados'), 404);
        }
    }

    public function find_get($id)
    {
        if (!$id) {
            $this->response(null, 400);
        }
        $disciplina_dia_hora = $this->disciplina_dia_hora_model->get($id);

        if (!is_null($disciplina_dia_hora)) {
            $this->response(array('Disciplina_dia_hora' => $disciplina_dia_hora), 200);
        } else {
            $this->response(array('error' => 'disciplina_dia_hora não encontrado'), 404);
        }
    }

    public function index_post()
    {
        if (!$this->post('disciplina_dia_hora')) {
            $this->response(null, 400);
        }

        $id = $this->disciplina_dia_hora_model->save($this->post('disciplina_dia_horae'));

        if (!is_null($id)) {
            $this->response(array('Disciplina_dia_hora' => $id), 200);
        } else {
            $this->response(array('Erro ao salvar disciplina_dia_hora!!!'), 400);
        }
    }

    public function index_put()
    {
        if (!$this->put('disciplina_dia_hora')) {
            $this->response(null, 400);
        }

        $update = $this->disciplina_dia_hora_model->update($this->put('disciplina_dia_hora'));

        if (!is_null($update)) {
            $this->response(array('Disciplina_dia_hora' => 'Dados disciplina_dia_hora atualizado!'), 200);
        } else {
            $this->response(array('error', 'Não foi possivel atualizar os dados ...'), 400);
        }
    }

    public function index_delete($id)
    {
        if (!$id) {
            $this->response(null, 400);
        }

        $delete = $this->disciplina_dia_hora_model->delete($id);

        if (!is_null($delete)) {
            $this->response(array('Disciplina_dia_hora' => 'Disciplina_dia_hora excluido'), 200);
        } else {
            $this->response(array('error', 'Não foi possivel excluir disciplina_dia_hora'), 400);
        }
    }
}
