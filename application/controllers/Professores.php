<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/libraries/REST_Controller.php';


class Professores extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('professores_model');
    }

    public function index_get()
    {
        $professores = $this->professores_model->get();

        if (!is_null($professores)) {
            $this->response(array('Professor' => $professores), 200);
        } else {
            $this->response(array('error' => 'Nao existe professores na base de dados'), 404);
        }
    }

    public function find_get($id)
    {
        if (!$id) {
            $this->response(null, 400);
        }
        $professores = $this->professores_model->get($id);

        if (!is_null($professores)) {
            $this->response(array('Professor' => $professores), 200);
        } else {
            $this->response(array('error' => 'professores não encontrado'), 404);
        }
    }

    public function index_post()
    {
        if (!$this->post('professor')) {
            $this->response(null, 400);
        }

        $id = $this->professores_model->save($this->post('professore'));

        if (!is_null($id)) {
            $this->response(array('Professor' => $id), 200);
        } else {
            $this->response(array('Erro ao salvar professor!!!'), 400);
        }
    }

    public function index_put()
    {
        if (!$this->put('professor')) {
            $this->response(null, 400);
        }

        $update = $this->professores_model->update($this->put('professor'));

        if (!is_null($update)) {
            $this->response(array('Professor' => 'Dados professor atualizado!'), 200);
        } else {
            $this->response(array('error', 'Não foi possivel atualizar os dados ...'), 400);
        }
    }

    public function index_delete($id)
    {
        if (!$id) {
            $this->response(null, 400);
        }

        $delete = $this->professores_model->delete($id);

        if (!is_null($delete)) {
            $this->response(array('Professor' => 'Professor excluido'), 200);
        } else {
            $this->response(array('error', 'Não foi possivel excluir professor'), 400);
        }
    }
}
