<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/libraries/REST_Controller.php';


class Disciplina extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('disciplina_model');
    }

    public function index_get()
    {
        $disciplinas = $this->disciplina_model->get();

        if (!is_null($disciplinas)) {
            $this->response(array('Disciplina' => $disciplinas), 200);
        } else {
            $this->response(array('error' => 'Nao existe disciplinas na base de dados'), 404);
        }
    }

    public function find_get($id)
    {
        if (!$id) {
            $this->response(null, 400);
        }
        $disciplinas = $this->disciplina_model->get($id);

        if (!is_null($disciplinas)) {
            $this->response(array('Disciplina' => $disciplinas), 200);
        } else {
            $this->response(array('error' => 'disciplinas não encontrado'), 404);
        }
    }

    public function index_post()
    {
        if (!$this->post('disciplina')) {
            $this->response(null, 400);
        }

        $id = $this->disciplina_model->save($this->post('disciplina'));

        if (!is_null($id)) {
            $this->response(array('Disciplina' => $id), 200);
        } else {
            $this->response(array('Erro ao salvar disciplina!!!'), 400);
        }
    }

    public function index_put()
    {
        if (!$this->put('disciplina')) {
            $this->response(null, 400);
        }

        $update = $this->disciplina_model->update($this->put('disciplina'));

        if (!is_null($update)) {
            $this->response(array('Disciplina' => 'Dados disciplina atualizado!'), 200);
        } else {
            $this->response(array('error', 'Não foi possivel atualizar os dados ...'), 400);
        }
    }

    public function index_delete($id)
    {
        if (!$id) {
            $this->response(null, 400);
        }

        $delete = $this->disciplina_model->delete($id);

        if (!is_null($delete)) {
            $this->response(array('Disciplina' => 'Disciplina excluido'), 200);
        } else {
            $this->response(array('error', 'Não foi possivel excluir disciplina'), 400);
        }
    }
}
