<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/libraries/REST_Controller.php';


class Salas extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('salas_model');
    }

    public function index_get()
    {
        $salas = $this->salas_model->get();

        if (!is_null($salas)) {
            $this->response(array('Sala' => $salas), 200);
        } else {
            $this->response(array('error' => 'Nao existe salas na base de dados'), 404);
        }
    }

    public function find_get($id)
    {
        if (!$id) {
            $this->response(null, 400);
        }
        $salas = $this->salas_model->get($id);

        if (!is_null($salas)) {
            $this->response(array('Sala' => $salas), 200);
        } else {
            $this->response(array('error' => 'Salas não encontrada'), 404);
        }
    }

    public function index_post()
    {
        if (!$this->post('sala')) {
            $this->response(null, 400);
        }

        $id = $this->salas_model->save($this->post('sala'));

        if (!is_null($id)) {
            $this->response(array('sala' => $id), 200);
        } else {
            $this->response(array('Erro ao salvar sala!!!'), 400);
        }
    }

    public function index_put()
    {
        if (!$this->put('sala')) {
            $this->response(null, 400);
        }

        $update = $this->salas_model->update($this->put('sala'));

        if (!is_null($update)) {
            $this->response(array('Sala' => 'Dados da sala não atualizado!'), 200);
        } else {
            $this->response(array('error', 'Não foi possivel atualizar os dados ...'), 400);
        }
    }

    public function index_delete($id)
    {
        if (!$id) {
            $this->response(null, 400);
        }

        $delete = $this->sala_model->delete($id);

        if (!is_null($delete)) {
            $this->response(array('Sala' => 'Sala excluida'), 200);
        } else {
            $this->response(array('error', 'Não foi possivel excluir a sala'), 400);
        }
    }
}
