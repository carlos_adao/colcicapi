<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/libraries/REST_Controller.php';


class Semestre_virgente extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('semestreVirgente_model');
    }

    public function index_get()
    {
        $sv = $this->semestreVirgente_model->get();

        if (!is_null($sv)) {
            $this->response(array('Semestre Virgente' => $sv), 200);
        } else {
            $this->response(array('error' => 'Nao existe Semestre Virgente na base de dados'), 404);
        }
    }

    public function find_get($id)
    {
        if (!$id) {
            $this->response(null, 400);
        }
        $sv = $this->semestreVirgente_model->get($id);

        if (!is_null($sv)) {
            $this->response(array('Semestre Virgente' => $sv), 200);
        } else {
            $this->response(array('error' => 'Semestre Virgente não encontrado'), 404);
        }
    }

    public function index_post()
    {
        if (!$this->post('semestre_virgente')) {
            $this->response(null, 400);
        }

        $id = $this->semestreVirgente_model->save($this->post('semestre_virgentee'));

        if (!is_null($id)) {
            $this->response(array('Semestre Virgente' => $id), 200);
        } else {
            $this->response(array('Erro ao salvar semestre_virgente!!!'), 400);
        }
    }

    public function index_put()
    {
        if (!$this->put('semestre_virgente')) {
            $this->response(null, 400);
        }

        $update = $this->semestreVirgente_model->update($this->put('semestre_virgente'));

        if (!is_null($update)) {
            $this->response(array('Semestre Virgente' => 'Dados semestre_virgente atualizado!'), 200);
        } else {
            $this->response(array('error', 'Não foi possivel atualizar os dados ...'), 400);
        }
    }

    public function index_delete($id)
    {
        if (!$id) {
            $this->response(null, 400);
        }

        $delete = $this->semestreVirgente_model->delete($id);

        if (!is_null($delete)) {
            $this->response(array('Semestre Virgente' => 'Semestre Virgente excluido'), 200);
        } else {
            $this->response(array('error', 'Não foi possivel excluir semestre_virgente'), 400);
        }
    }
}
