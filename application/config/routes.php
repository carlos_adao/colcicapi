<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
| example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
| http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
| $route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
| $route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
| $route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples: my-controller/index -> my_controller/index
$route['default_controller'] = 'professores';
|   my-controller/my-method -> my_controller/my_method
*/
$route['404_override'] = '';
$route['translate_uri_dashes'] = TRUE;

//Routes for professores
$route['professores']['get'] = 'professores/index';
$route['professores/(:num)']['get'] = 'professores/find/$1';
$route['professores']['post'] = 'professores/index';
$route['professores/(:num)']['put'] = 'professores/index/$1';
$route['professores/(:num)']['delete'] = 'professores/index/$1';

//Routes for Salas
$route['salas']['get'] = 'salas/index';
$route['salas/(:num)']['get'] = 'salas/find/$1';
$route['salas']['post'] = 'salas/index';
$route['salas/(:num)']['put'] = 'salas/index/$1';
$route['salas/(:num)']['delete'] = 'salas/index/$1';

//Routes for Funcionarios
$route['funcionarios']['get'] = 'funcionarios/index';
$route['funcionarios/(:num)']['get'] = 'funcionarios/find/$1';
$route['funcionarios']['post'] = 'funcionarios/index';
$route['funcionarios/(:num)']['put'] = 'funcionarios/index/$1';
$route['funcionarios/(:num)']['delete'] = 'funcionarios/index/$1';

//Routes for Semestre_virgente
$route['semestre_virgente']['get'] = 'semestre_virgente/index';
$route['semestre_virgente/(:num)']['get'] = 'semestre_virgente/find/$1';
$route['semestre_virgente']['post'] = 'semestre_virgente/index';
$route['semestre_virgente/(:num)']['put'] = 'semestre_virgente/index/$1';
$route['semestre_virgente/(:num)']['delete'] = 'semestre_virgente/index/$1';

//Routes for Disciplina
$route['disciplina']['get'] = 'disciplina/index';
$route['disciplina/(:num)']['get'] = 'disciplina/find/$1';
$route['disciplina']['post'] = 'disciplina/index';
$route['disciplina/(:num)']['put'] = 'disciplina/index/$1';
$route['disciplina/(:num)']['delete'] = 'disciplina/index/$1';

//Routes for Ementa
$route['ementa']['get'] = 'ementa/index';
$route['ementa/(:num)']['get'] = 'ementa/find/$1';
$route['ementa']['post'] = 'ementa/index';
$route['ementa/(:num)']['put'] = 'ementa/index/$1';
$route['ementa/(:num)']['delete'] = 'ementa/index/$1';

//Routes for Disciplina_dia_hora
$route['disciplina_dia_hora']['get'] = 'disciplina_dia_hora/index';
$route['disciplina_dia_hora/(:num)']['get'] = 'disciplina_dia_hora/find/$1';
$route['disciplina_dia_hora']['post'] = 'disciplina_dia_hora/index';
$route['disciplina_dia_hora/(:num)']['put'] = 'disciplina_dia_hora/index/$1';
$route['disciplina_dia_hora/(:num)']['delete'] = 'disciplina_dia_hora/index/$1';


//Routes for Settings
$route['settings']['get'] = 'settings/index';
$route['settings/(:num)']['get'] = 'settings/find/$1';
$route['settings']['post'] = 'settings/index';
$route['settings/(:num)']['put'] = 'settings/index/$1';
$route['settings/(:num)']['delete'] = 'settings/index/$1';

// Routes for cities
$route['cities']['get'] = 'cities/index';
$route['cities/(:num)']['get'] = 'cities/find/$1';
$route['cities']['post'] = 'cities/index';
$route['cities/(:num)']['put'] = 'cities/index/$1';
$route['cities/(:num)']['delete'] = 'cities/index/$1';



// Routes for forecast
$route['forecast']['get'] = 'forecast/index';
$route['forecast/(:num)']['get'] = 'forecast/find/$1';
$route['forecast']['post'] = 'forecast/index';
$route['forecast/(:num)']['put'] = 'forecast/index/$1';
$route['forecast/(:num)']['delete'] = 'forecast/index/$1';

/*
| -------------------------------------------------------------------------
| Sample REST API Routes
| -------------------------------------------------------------------------
*/
//$route['api/example/users/(:num)'] = 'api/example/users/id/$1'; // Example 4
//$route['api/example/users/(:num)(\.)([a-zA-Z0-9_-]+)(.*)'] = 'api/example/users/id/$1/format/$3$4'; // Example 8
