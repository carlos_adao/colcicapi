<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Salas_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get($id = null)
    {
        if (!is_null($id)) {
            $query = $this->db->select('*')->from('sala')->where('codigo', $id)->get();
            if ($query->num_rows() === 1) {
                return $query->row_array();
            }

            return null;
        }

        $query = $this->db->select('*')->from('sala')->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }

        return null;
    }

    public function save($sala)
    {
        $this->db->set($this->_setSala($sala))->insert('sala');

        if ($this->db->affected_rows() === 1) {
            return $this->db->insert_id();
        }

        return null;
    }

    public function update($sala)
    {
        $id = $sala['codigo'];

        $this->db->set($this->_setSala($sala))->where('codigo', $id)->update('sala');

        if ($this->db->affected_rows() === 1) {
            return true;
        }

        return null;
    }

    public function delete($id)
    {
        $this->db->where('codigo', $id)->delete('sala');

        if ($this->db->affected_rows() === 1) {
            return true;
        }

        return null;
    }

    private function _setSala($sala)
    {
        return array(
            'codigo' =>$professor['codigo'],
            'localizacao'   => $professor['localizacao'],
            'caracteristica'   => $professor['caracteristica']
        );
    }
}
