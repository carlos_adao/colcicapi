<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Disciplina_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get($id = null)
    {
        if (!is_null($id)) {
            $query = $this->db->select('*')->from('disciplina')->where('codigo', $id)->get();
            if ($query->num_rows() === 1) {
                return $query->row_array();
            }

            return null;
        }

        $query = $this->db->select('*')->from('disciplina')->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }

        return null;
    }

    public function save($disciplina)
    {
        $this->db->set($this->_setDisciplina($disciplina))->insert('disciplina');

        if ($this->db->affected_rows() === 1) {
            return $this->db->insert_id();
        }

        return null;
    }

    public function update($disciplina)
    {
        $id = $disciplina['codigo'];

        $this->db->set($this->_setDisciplina($disciplina))->where('codigo', $id)->update('disciplina');

        if ($this->db->affected_rows() === 1) {
            return true;
        }

        return null;
    }

    public function delete($id)
    {
        $this->db->where('codigo', $id)->delete('disciplina');

        if ($this->db->affected_rows() === 1) {
            return true;
        }

        return null;
    }

    private function _setDisciplina($disciplina){

        return array(
            'codigo' =>$disciplina['codigo'],
            'nome'   => $disciplina['nome'],
            'carga_horaria'   => $disciplina['carga_horaria'],
            'fk_semestre_virgente'   => $disciplina['fk_semestre_virgente'],
            'semestre_diciplina'   => $disciplina['semestre_diciplina'],
            'fk_curso'   => $disciplina['fk_curso'],
            'fk_professor'   => $disciplina['fk_professor'],
            'turma'   => $disciplina['turma'],
            'CHS'   => $disciplina['CHS']
        );
    }
}
