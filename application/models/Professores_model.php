<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Professores_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get($id = null)
    {
        if (!is_null($id)) {
            $query = $this->db->select('*')->from('professor')->where('codigo', $id)->get();
            if ($query->num_rows() === 1) {
                return $query->row_array();
            }

            return null;
        }

        $query = $this->db->select('*')->from('professor')->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }

        return null;
    }

    public function save($professor)
    {
        $this->db->set($this->_setProfessor($professor))->insert('professor');

        if ($this->db->affected_rows() === 1) {
            return $this->db->insert_id();
        }

        return null;
    }

    public function update($professor)
    {
        $id = $professor['codigo'];

        $this->db->set($this->_setProfessor($professor))->where('codigo', $id)->update('professor');

        if ($this->db->affected_rows() === 1) {
            return true;
        }

        return null;
    }

    public function delete($id)
    {
        $this->db->where('codigo', $id)->delete('professor');

        if ($this->db->affected_rows() === 1) {
            return true;
        }

        return null;
    }

    private function _setProfessor($professor)
    {
        return array(
            'codigo' =>$professor['codigo'],
            'nome'   => $professor['nome'],
            'email'   => $professor['email'],
            'tel'   => $professor['tel'],
            'titulacao'   => $professor['titulacao'],
            'classe'   => $professor['classe'],
            'url_lattes'   => $professor['url_lattes']
        );
    }
}
