<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ementa_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get($id = null)
    {
        if (!is_null($id)) {
            $query = $this->db->select('*')->from('ementa_disciplina')->where('codigo', $id)->get();
            if ($query->num_rows() === 1) {
                return $query->row_array();
            }

            return null;
        }

        $query = $this->db->select('*')->from('ementa_disciplina')->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }

        return null;
    }

    public function save($ementa_disciplina)
    {
        $this->db->set($this->_setEmenta($ementa_disciplina))->insert('ementa_disciplina');

        if ($this->db->affected_rows() === 1) {
            return $this->db->insert_id();
        }

        return null;
    }

    public function update($ementa_disciplina)
    {
        $id = $ementa_disciplina['codigo'];

        $this->db->set($this->_setEmenta($ementa_disciplina))->where('codigo', $id)->update('ementa_disciplina');

        if ($this->db->affected_rows() === 1) {
            return true;
        }

        return null;
    }

    public function delete($id)
    {
        $this->db->where('codigo', $id)->delete('ementa_disciplina');

        if ($this->db->affected_rows() === 1) {
            return true;
        }

        return null;
    }

    private function _setEmenta($ementa_disciplina)
    {
        return array(
            'fk_disicplina' =>$ementa_disciplina['fk_disicplina'],
            'fk_pre_requisito'   => $ementa_disciplina['fk_pre_requisito'],
            'ementa'   => $ementa_disciplina['ementa'],
            'objetivo'   => $ementa_disciplina['objetivo'],
            'metodologia'   => $ementa_disciplina['metodologia'],
            'avaliacao'   => $ementa_disciplina['avaliacao'],
            'conteudo_programatico'   => $ementa_disciplina['conteudo_programatico'],
            'referencia'   => $ementa_disciplina['referencia']
        );
    }
}
