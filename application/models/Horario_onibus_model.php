<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Horario_onibus_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get($cod = null)
    {
        if (!is_null($cod)) {
            $query = $this->db->select('*')->from('h_salobrinho_atacadao')->where('cod', $cod)->get();
            if ($query->num_rows() === 1) {
                return $query->row_array();
            }

            return null;
        }

        $query = $this->db->select('*')->from('h_salobrinho_atacadao')->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }

        return null;
    }

    public function save($horario)
    {
        $this->db->set($this->_setHorarioOnibus($horario))->insert('h_salobrinho_atacadao');

        if ($this->db->affected_rows() === 1) {
            return $this->db->insert_cod();
        }

        return null;
    }

    public function update($horario)
    {
        $cod = $horario['cod'];

        $this->db->set($this->_setHorarioOnibus($horario))->where('cod', $cod)->update('h_salobrinho_atacadao');

        if ($this->db->affected_rows() === 1) {
            return true;
        }

        return null;
    }

    public function delete($cod)
    {
        $this->db->where('cod', $cod)->delete('h_salobrinho_atacadao');

        if ($this->db->affected_rows() === 1) {
            return true;
        }

        return null;
    }

    private function _setHorarioOnibus($horario)
    {
        return array(
            'name' => $horario['name']
        );
    }
}
