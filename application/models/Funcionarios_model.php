<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Funcionarios_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get($id = null)
    {
        if (!is_null($id)) {
            $query = $this->db->select('*')->from('funcionario')->where('codigo', $id)->get();
            if ($query->num_rows() === 1) {
                return $query->row_array();
            }

            return null;
        }

        $query = $this->db->select('*')->from('funcionario')->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }

        return null;
    }

    public function save($funcionario)
    {
        $this->db->set($this->_setFuncionario($funcionario))->insert('funcionario');

        if ($this->db->affected_rows() === 1) {
            return $this->db->insert_id();
        }

        return null;
    }

    public function update($funcionario)
    {
        $id = $funcionario['codigo'];

        $this->db->set($this->_setFuncionario($funcionario))->where('codigo', $id)->update('funcionario');

        if ($this->db->affected_rows() === 1) {
            return true;
        }

        return null;
    }

    public function delete($id)
    {
        $this->db->where('codigo', $id)->delete('funcionario');

        if ($this->db->affected_rows() === 1) {
            return true;
        }

        return null;
    }

    private function _setFuncionario($funcionario)
    {
        return array(
            'codigo' =>$funcionario['codigo'],
            'nome'   => $funcionario['nome'],
            'email'   => $funcionario['email'],
            'tel'   => $funcionario['tel'],
            'cargo'   => $funcionario['cargo'],
            'hierarquia'   => $funcionario['hierarquia'],
            'sexo'   => $funcionario['sexo']
        );
    }
}
