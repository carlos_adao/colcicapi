<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SemestreVirgente_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get($id = null)
    {
        if (!is_null($id)) {
            $query = $this->db->select('*')->from('semestre_virgente')->where('codigo', $id)->get();
            if ($query->num_rows() === 1) {
                return $query->row_array();
            }

            return null;
        }

        $query = $this->db->select('*')->from('semestre_virgente')->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }

        return null;
    }

    public function save($semestre_virgente)
    {
        $this->db->set($this->_setSemestreVirgente($semestre_virgente))->insert('semestre_virgente');

        if ($this->db->affected_rows() === 1) {
            return $this->db->insert_id();
        }

        return null;
    }

    public function update($semestre_virgente)
    {
        $id = $semestre_virgente['codigo'];

        $this->db->set($this->_setSemestreVirgente($semestre_virgente))->where('codigo', $id)->update('semestre_virgente');

        if ($this->db->affected_rows() === 1) {
            return true;
        }

        return null;
    }

    public function delete($id)
    {
        $this->db->where('codigo', $id)->delete('semestre_virgente');

        if ($this->db->affected_rows() === 1) {
            return true;
        }

        return null;
    }

    private function _setSemestreVirgente($semestre_virgente)
    {
        return array(
            'codigo' =>$semestre_virgente['codigo'],
            'ano'   => $semestre_virgente['nome'],
            'periodo'   => $semestre_virgente['periodo']
        );
    }
}
