<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Disciplina_dia_hora_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get($id = null)
    {
        if (!is_null($id)) {
            $query = $this->db->select('*')->from('disciplina_dia_hora')->where('codigo', $id)->get();
            if ($query->num_rows() === 1) {
                return $query->row_array();
            }

            return null;
        }

        $query = $this->db->select('*')->from('disciplina_dia_hora')->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }

        return null;
    }

    public function save($disciplina_dia_hora)
    {
        $this->db->set($this->_setDisciplinaDiaHora($disciplina_dia_hora))->insert('disciplina_dia_hora');

        if ($this->db->affected_rows() === 1) {
            return $this->db->insert_id();
        }

        return null;
    }

    public function update($disciplina_dia_hora)
    {
        $id = $disciplina_dia_hora['codigo'];

        $this->db->set($this->_setDisciplinaDiaHora($disciplina_dia_hora))->where('codigo', $id)->update('disciplina_dia_hora');

        if ($this->db->affected_rows() === 1) {
            return true;
        }

        return null;
    }

    public function delete($id)
    {
        $this->db->where('codigo', $id)->delete('disciplina_dia_hora');

        if ($this->db->affected_rows() === 1) {
            return true;
        }

        return null;
    }

    private function _setDisciplinaDiaHora($disciplina_dia_hora)
    {
        return array(
            'id' =>$disciplina_dia_hora['id'],
            'fk_codigo'   => $disciplina_dia_hora['fk_codigo'],
            'fk_turma'   => $disciplina_dia_hora['fk_turma'],
            'fk_dia_hora'   => $disciplina_dia_hora['fk_dia_hora'],
            'fk_sala'   => $disciplina_dia_hora['fk_sala']
        );
    }
}
